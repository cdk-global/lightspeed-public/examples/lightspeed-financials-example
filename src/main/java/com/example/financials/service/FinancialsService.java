package com.example.financials.service;

import com.example.financials.dto.Financials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class FinancialsService {

    private static final Logger log = LoggerFactory.getLogger(FinancialsService.class);

    public Financials saveFinancials(Financials financials) {

        //TODO save the financial data in your system

        return financials;
    }
}
