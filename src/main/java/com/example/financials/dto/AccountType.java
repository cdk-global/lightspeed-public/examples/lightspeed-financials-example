package com.example.financials.dto;

/**
 * @author David Horton
 * Date:   3/17/21
 */
public enum AccountType {
    NONE,
    A, //Asset
    L, //Liability
    O, //Owner's Equity
    R, //Revenue
    E, //Expenses
    C //Cost of goods
}
