package com.example.financials.dto;

/**
 * @author David Horton
 * Date:   3/17/21
 */
public class FinancialLine {

    private String accountID;
    private String accountDesc;
    private AccountType accountType = AccountType.NONE;
    private AccountCategory category = AccountCategory.NONE;
    private EntryType entryType = EntryType.NONE;
    private AccountOffset offset = AccountOffset.NONE;
    private double monthEndAmount;
    private double priorMonthAmount;
    private Currency currency = Currency.USD;
    private int monthEndCount;
    private int priorMonthCount;

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getAccountDesc() {
        return accountDesc;
    }

    public void setAccountDesc(String accountDesc) {
        this.accountDesc = accountDesc;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public AccountCategory getCategory() {
        return category;
    }

    public void setCategory(AccountCategory category) {
        this.category = category;
    }

    public EntryType getEntryType() {
        return entryType;
    }

    public void setEntryType(EntryType entryType) {
        this.entryType = entryType;
    }

    public AccountOffset getOffset() {
        return offset;
    }

    public void setOffset(AccountOffset offset) {
        this.offset = offset;
    }

    public double getMonthEndAmount() {
        return monthEndAmount;
    }

    public void setMonthEndAmount(double monthEndAmount) {
        this.monthEndAmount = monthEndAmount;
    }

    public double getPriorMonthAmount() {
        return priorMonthAmount;
    }

    public void setPriorMonthAmount(double priorMonthAmount) {
        this.priorMonthAmount = priorMonthAmount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public int getMonthEndCount() {
        return monthEndCount;
    }

    public void setMonthEndCount(int monthEndCount) {
        this.monthEndCount = monthEndCount;
    }

    public int getPriorMonthCount() {
        return priorMonthCount;
    }

    public void setPriorMonthCount(int priorMonthCount) {
        this.priorMonthCount = priorMonthCount;
    }
}
