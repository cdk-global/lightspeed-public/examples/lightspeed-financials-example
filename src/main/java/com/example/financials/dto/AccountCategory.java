package com.example.financials.dto;

/**
 * @author David Horton
 * Date:   3/17/21
 */
public enum AccountCategory {
    NONE,
    C, //Consolidated
    D //Detailed
}
