package com.example.financials.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author David Horton
 * Date:   3/16/21
 */
public class Financials {

    private String submittingUserName;
    private String lightSpeedDealerNumber;
    private String dealerIdentifier;
    private int month;
    private int year;
    private List<FinancialLine> financials = new ArrayList<>();

    public String getSubmittingUserName() {
        return submittingUserName;
    }

    public void setSubmittingUserName(String submittingUserName) {
        this.submittingUserName = submittingUserName;
    }

    public String getLightSpeedDealerNumber() {
        return lightSpeedDealerNumber;
    }

    public void setLightSpeedDealerNumber(String lightSpeedDealerNumber) {
        this.lightSpeedDealerNumber = lightSpeedDealerNumber;
    }

    public String getDealerIdentifier() {
        return dealerIdentifier;
    }

    public void setDealerIdentifier(String dealerIdentifier) {
        this.dealerIdentifier = dealerIdentifier;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<FinancialLine> getFinancials() {
        return financials;
    }

    public void setFinancials(List<FinancialLine> financials) {
        this.financials = financials;
    }
}
