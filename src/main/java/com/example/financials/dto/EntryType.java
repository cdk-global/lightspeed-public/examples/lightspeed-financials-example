package com.example.financials.dto;

/**
 * @author David Horton
 * Date:   3/17/21
 */
public enum EntryType {
    NONE,
    C, //Credit
    D //Debit
}
