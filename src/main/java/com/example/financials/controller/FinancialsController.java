package com.example.financials.controller;

import com.example.financials.dto.Financials;
import com.example.financials.service.FinancialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FinancialsController {

    private final FinancialsService purchaseOrderService;

    @Autowired
    public FinancialsController(FinancialsService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    @GetMapping("/test")
    public ResponseEntity<String> getTestStatus() {
        return new ResponseEntity<>("Up and running", HttpStatus.OK);
    }

    @PostMapping("/financials")
    public ResponseEntity<Financials> saveFinancials(@RequestBody Financials financials) {
        return new ResponseEntity<>(purchaseOrderService.saveFinancials(financials), HttpStatus.CREATED);
    }

}
